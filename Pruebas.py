'''
Created on 5 dic. 2020

@author: Rafael Villaneda
'''
from random import *
import time
import pickle
import os
class MetodosOrdenamientos:
    def __init__(self):
        self.contador=[0,0,0]
    def mostrarContador(self):
        #1-> recorridos 2-> intercambios 3-> comparaciones
        print(f"Numeros de recorridos: {self.contador[0]}")
        print(f"Numeros de intercambios: {self.contador[1]}")
        print(f"Numeros de Comparaciones: {self.contador[2]}")
        self.contador[0]=0
        self.contador[1]=0
        self.contador[2]=0
        
    def mezclaDirecta(self,array): 
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                self.contador[2]=self.contador[2]+1
                if(arregloIz[0]< arregloDer[0]):
                    self.contador[1]=self.contador[1]+1
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
                self.contador[0]=self.contador[0]+1
            while len(arregloIz)>0:
                self.contador[1]=self.contador[1]+1
                array.append(arregloIz.pop(0))
                self.contador[0]=self.contador[0]+1
            
            while len(arregloDer)>0:
                self.contador[1]=self.contador[1]+1
                array.append(arregloDer.pop(0))
                self.contador[0]=self.contador[0]+1
        
        return array
    def mezclaDirecta2(self,array): 
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                self.contador[2]=self.contador[2]+1
                if(arregloIz[0]< arregloDer[0]):
                    self.contador[1]=self.contador[1]+1
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
                self.contador[0]=self.contador[0]+1
            while len(arregloIz)>0:
                self.contador[1]=self.contador[1]+1
                array.append(arregloIz.pop(0))
                self.contador[0]=self.contador[0]+1
            
            while len(arregloDer)>0:
                self.contador[1]=self.contador[1]+1
                array.append(arregloDer.pop(0))
                self.contador[0]=self.contador[0]+1
        
    def mezclaNatural(self, numeros):
        izquerdo = 0
        izq = 0
        derecho = len(numeros)-1
        der = derecho
        ordenado=False
        
        while(not ordenado):
            ordenado = True
            izquierdo =0
            while(izquierdo<derecho):
                izq=izquerdo
                while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                    izq=izq+1
                der=izq+1
                while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                    der=der+1
                if(der<=derecho):
                    self.mezclaDirecta2(numeros)
                    ordenado= False
                izquierdo = izq
    
    @staticmethod
    def array(a1:list, a2:list) -> list:
        intercambios=0
        comparaciones=0
        recorridos=0
        a3 : list = []
        cont : int = 0
        cont2 : int = 0

        while len(a1)!=cont and len(a2)!=cont2:
            comparaciones=comparaciones+1
            recorridos=recorridos+1
            comparaciones=comparaciones+1
            if a1[cont]<=a2[cont2]:
                intercambios=intercambios
                a3.append(a1[cont])
                cont+=1
            else:
                a3.append(a2[cont2])
                cont2+=1

        while len(a1)!=cont:
            comparaciones=comparaciones+1
            a3.append(a1[cont])
            cont+=1
            recorridos=recorridos+1

        while len(a2)!=cont2:
            comparaciones=comparaciones+1
            a3.append(a2[cont2])
            cont2+=1
            recorridos=recorridos+1
        
        print(f"Numeros de recorridos: {recorridos}")
        print(f"Numeros de intercambios: {comparaciones}")
        print(f"Numeros de Comparaciones: {comparaciones}")
        return a3

class PruebasEstres:
    def pruebaEstres(self,op):
        vector1000E=[randint(0,1000) for i in range(1000)]
        vector10000E=[randint(0,10000) for i in range(10000)]
        vector100000E=[randint(0,100000) for i in range(100000)]
        vector100000E=[randint(0,100000) for i in range(100000)]
        vector1=[10,12,5,2,8,3,1,7,22,6,30,4,11,2,9]
        if(op=="1"):
            self.pruebas(vector1)
        elif(op=="2"):
            self.pruebas(vector1000E)
        elif(op=="3"):
            self.pruebas(vector10000E)
        elif(op=="4"):
            self.pruebas(vector100000E)
        else:
            print("Elejiste una opcion incorrecta")
        pass
    def pruebas(self,vector):
        ordenar=MetodosOrdenamientos()
        copi=vector.copy()
        print("-------------------- Intercalacion ----------------------------------")
        v1=[]
        v2=[]
        n1=randint(1,len(copi))
        n2=len(copi)-n1
        for i in range(0,n1):
            v1.append(copi.pop(0))
        for i in range(0,n2):
            v2.append(copi.pop(0))
        v1=ordenar.mezclaDirecta(v1)
        v2=ordenar.mezclaDirecta(v2)
        tiempoIn=tFin=0
        print(v1)
        print(v2)
        tiempoIn=time.time()
        ordenar.array(v1,v2)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        copi.clear()
        copi=vector.copy()
        print("-------------------- Mezcla directa ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.mezclaDirecta(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("-------------------- Mezcla Natural ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.mezclaNatural(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()

#----------------------------------------
bandera=False
prueba=PruebasEstres()
while (bandera==False):
    print("Con que quieres probar?")
    print("1-> Ordenar elementos precargados")
    print("2-> Ordenar 1000 elementos")
    print("3-> Ordenar 10000 elementos")
    print("4-> Ordenar 100000 elementos")
    print("5-> Salir")
    op=input()
    if(op=="1"):
        prueba.pruebaEstres(op)
    elif(op=="2"):
        prueba.pruebaEstres(op)
    elif(op=="3"):
        prueba.pruebaEstres(op)
    elif(op=="4"):
        prueba.pruebaEstres(op)
    elif(op=="5"):
        print("Saliendo....")
        bandera=True